#include "stdafx.h"
#include "Component.h"
#include "Composite.h"


Component::Component()
{
	parent = NULL;

	pos = { 0,0,0 };
	scale = { 1,1,1 };
	rot = { 0,0,0 };

	D3DXMatrixIdentity(&ModelMat);
	D3DXMatrixIdentity(&scaleMat);
	D3DXMatrixIdentity(&rotXMat);
	D3DXMatrixIdentity(&rotYMat);
	D3DXMatrixIdentity(&rotZMat);
	D3DXMatrixIdentity(&translateMat);

	forward = { 0,0,1 };
	right = { 1,0,0 };
	up = { 0,1,0 };

}


Component::~Component()
{
	
}

void Component::Update()
{
}

void Component::Render()
{
}

void Component::Render(D3DXMATRIX* parent)
{
}

void Component::Move(D3DXVECTOR3 dir)
{
	pos += dir;

	bb = bb.Transform(*UpdateMatrix());
	if (parent)
		parent->UpdateParentBBox();
}

void Component::Rotate(D3DXVECTOR3 angles)
{
	rot += angles;

	BoundingBox aux;
	aux = aux.Transform(*UpdateMatrix());
	bb = aux;
	if (parent)
		parent->UpdateParentBBox();
}

void Component::Scale(D3DXVECTOR3 mag)
{
	scale += mag;	

	BoundingBox aux;
	aux = aux.Transform(*UpdateMatrix());
	bb = aux;
	if (parent)
		parent->UpdateParentBBox();
}

void Component::SetPosition(D3DXVECTOR3 _pos)
{
	pos = _pos;

	bb = bb.Transform(*UpdateMatrix());
	if (parent)
		parent->UpdateParentBBox();
}

void Component::SetRotation(D3DXVECTOR3 angles)
{
	rot = angles;

	BoundingBox aux;
	aux = aux.Transform(*UpdateMatrix());
	bb = aux;
	if (parent)
		parent->UpdateParentBBox();
}

void Component::SetScale(D3DXVECTOR3 mag)
{
	scale = mag;

	BoundingBox aux;
	aux = aux.Transform(*UpdateMatrix());
	bb = aux;
	if (parent)
		parent->UpdateParentBBox();
}

D3DXVECTOR3 Component::GetPosition()
{
	return pos;
}

D3DXVECTOR3 Component::GetRotation()
{
	return rot;
}

D3DXVECTOR3 Component::GetScale()
{
	return scale;
}

void Component::SetParent(Composite * parent)
{
	this->parent = parent;
	this->parent->UpdateParentBBox();
}

Composite * Component::GetParent()
{
	return parent;
	
}

D3DXMATRIX* Component::UpdateMatrix()
{

	//Translate Matrix
	translateMat._41 = pos.x;
	translateMat._42 = pos.y;
	translateMat._43 = pos.z;

	//Scale Matrix
	scaleMat._11 = scale.x;
	scaleMat._22 = scale.y;
	scaleMat._33 = scale.z;

	//Rotations Matrix
	rotXMat._22 = cos(rot.x);
	rotXMat._23 = sin(rot.x);
	rotXMat._32 = -sin(rot.x);
	rotXMat._33 = cos(rot.x);
	
	rotYMat._11 = cos(rot.y);
	rotYMat._31 = sin(rot.y);
	rotYMat._13 = -sin(rot.y);
	rotYMat._33 = cos(rot.y);
	
	rotZMat._11 = cos(rot.z);
	rotZMat._21 = sin(rot.z);
	rotZMat._12 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);
	//D3DXMatrixRotationX(&rotXMat, D3DXToRadian(rot.x));
	//D3DXMatrixRotationY(&rotYMat, D3DXToRadian(rot.y));
	//D3DXMatrixRotationZ(&rotZMat, D3DXToRadian(rot.z));

	//Forward Right Up
	D3DXVECTOR3 wForward(0, 0, 1);
	D3DXVECTOR3 wRight(1, 0, 0);
	D3DXVECTOR3 wUp(0, 1, 0);

	D3DXVECTOR4 objForward;
	D3DXVECTOR4 objRight;
	D3DXVECTOR4 objUp;

	D3DXVec3Transform(&objForward, &wForward, &(rotXMat * rotYMat * rotXMat));
	forward = { objForward.x, objForward.y, objForward.z };

	D3DXVec3Transform(&objRight, &wRight, &(rotXMat * rotYMat * rotXMat));
	right = { objRight.x, objRight.y, objRight.z };

	D3DXVec3Transform(&objUp, &wUp, &(rotXMat * rotYMat * rotXMat));
	up = { objUp.x, objUp.y, objUp.z };

	//Model Matrix
	ModelMat = (scaleMat * (rotXMat * rotYMat * rotZMat) * translateMat);

	return &ModelMat;
}

D3DXVECTOR3 Component::GetForward()
{
	return forward;
}

D3DXVECTOR3 Component::GetRight()
{
	return right;
}

D3DXVECTOR3 Component::GetUp()
{
	return up;
}

BoundingBox Component::GetBoundingBox()
{
	return bb;
}

BoundingBox Component::GetCombinedBBox(D3DXMATRIX parentModelMat)
{
	bb = bb.Transform(parentModelMat * *UpdateMatrix());
	return bb;
}

//void Component::UpdateParentBBox()
//{
//	parent->UpdateParentBBox();
//}

void Component::SeekImportantBB()
{
}

