

#include "stdafx.h"
#include "Model.h"
#include "Game.h"
#include "BoundingBox.h"

/*Model::Model(std::string _tag, LPDIRECT3DDEVICE9 dev)
{
	vertexCount = 4;
	indexCount = 6;
	tag = _tag;

	Vertex vertexes[] =
	{
		{ -0.5f, 0.5f, 0.0f, 0.0f, 1.0f },
		{ 0.5f, 0.5f, 0.0f, 1.0f, 1.0f },
		{ 0.5f, -0.5f, 0.0f, 1.0f, 0.0f },
		{ -0.5f, -0.5f, 0.0f, 0.0f, 0.0f },
	};

	WORD indexes[] = { 0,1,2,2,3,0 };
	
	dev->CreateVertexBuffer(
		vertexCount * sizeof(Vertex),
		D3DUSAGE_WRITEONLY,
		CUSTOMFVF,
		D3DPOOL_MANAGED,
		&vb,
		NULL);

	dev->CreateIndexBuffer(
		indexCount * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&ib,
		NULL);

	
	VOID* data;

	vb->Lock(0, 0, &data, 0);
	memcpy(data, vertexes, vertexCount * sizeof(Vertex));
	vb->Unlock();

	ib->Lock(0, 0, &data, 0);
	memcpy(data, indexes, indexCount * sizeof(WORD));
	ib->Unlock();
	
}
*/
/*Model::Model(std::vector<Vertex>_vertexes, std::vector<int> _indexes, std::string _tag)
{
	vertexCount = _vertexes.max_size();
	indexCount = _indexes.max_size();
	tag = _tag;

	Vertex vertexes[] =
	{
		{ -0.5f, 0.5f, 0.0f, 0.0f, 2.0f },
		{ 0.5f, 0.5f, 0.0f, 2.0f, 2.0f },
		{ 0.5f, -0.5f, 0.0f, 2.0f, 0.0f },
		{ -0.5f, -0.5f, 0.0f, 0.0f, 0.0f },
	};

	Game auxGame;
	auxGame.CreateBuffers(&vb, vertexCount, &ib, indexCount);
	
	/*
	VOID* data;

	vb->Lock(0, 0, &data, 0);
	memcpy(data, _vertexes, vertexCount * sizeof(Vertex));
	vb->Unlock();

	ib->Lock(0, 0, &data, 0);
	memcpy(data, _indexes, indexCount * sizeof(WORD));
	ib->Unlock();
	
}
*/

Model::Model(std::string path, std::string _tag)
{
	LoadModel(path);
	vertexCount = vertexes.size();
	indexCount = indexes.size();
	tag = _tag;


	LPDIRECT3DDEVICE9 dev = Game::GetInstance()->GetDevice();
	Game::GetInstance()->GetDevice()->CreateVertexBuffer(vertexes.size() * sizeof(Vertex), D3DUSAGE_WRITEONLY, CUSTOMFVF, D3DPOOL_MANAGED, &vb, NULL);
	Game::GetInstance()->GetDevice()->CreateIndexBuffer(indexes.size() * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib, NULL);

	VOID* lockedData = NULL;
	vb->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, vertexes.data(), vertexes.size() * sizeof(Vertex));
	vb->Unlock();

	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes.data(), indexes.size() * sizeof(WORD));
	ib->Unlock();
}

bool Model::LoadModel(std::string path)
{
	std::vector<D3DXVECTOR3> positions;
	std::vector<D3DXVECTOR3> normals;
	std::vector<D3DXVECTOR2> uvs;

	FILE* file;
	fopen_s(&file, path.data(), "r");


	while (!feof(file))
	{
		char lineHeader[128];
		fscanf(file, "%s", lineHeader);

		if (strcmp(lineHeader, "v") == 0)
		{
			D3DXVECTOR3 position;
			fscanf(file, "%f %f %f\n", &position.x, &position.y, &position.z);

			bb.xMin = min(position.x, bb.xMin);
			bb.yMin = min(position.y, bb.yMin);
			bb.zMin = min(position.z, bb.zMin);

			bb.xMax = max(position.x, bb.xMax);
			bb.yMax = max(position.y, bb.yMax);
			bb.zMax = max(position.z, bb.zMax);

			positions.push_back(position);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			D3DXVECTOR2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uvs.push_back(uv);
		}

		else if (strcmp(lineHeader, "vn") == 0)
		{
			D3DXVECTOR3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			Vertex vertex;
			int posIndex, uvIndex, normalIndex, indice;

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;

			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;

			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;

			if (IsVertexLoaded(vertex, &indice))
				indexes.push_back(indice);
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}


			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;

			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;

			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;

			if (IsVertexLoaded(vertex, &indice))
				indexes.push_back(indice);
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}

			fscanf(file, "%d/%d/%d\n", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;

			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;

			if (IsVertexLoaded(vertex, &indice))
				indexes.push_back(indice);
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}
		}
	}

	bb.Refresh();

	return true;
}

bool Model::IsVertexLoaded(Vertex vertex, int* indice)
{
	for (int i = 0; i < vertexes.size(); i++)
	{
		if (vertexes[i].x == vertex.x)
		{
			if (vertexes[i].y == vertex.y)
			{
				if (vertexes[i].z == vertex.z)
				{
					if (vertexes[i].nx == vertex.nx)
					{
						if (vertexes[i].ny == vertex.ny)
						{
							if (vertexes[i].nz == vertex.nz)
							{
								if (vertexes[i].tu == vertex.tu)
								{
									if (vertexes[i].tv == vertex.tv)
									{
										*indice = i;
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return false;
}

Model::~Model()
{
}

int Model::GetVertexCount()
{
	return vertexCount;
}

int Model::GetIndexCount()
{
	return indexCount;
}

std::string Model::GetTag()
{
	return tag;
}

LPDIRECT3DVERTEXBUFFER9 Model::GetVertexBuffer()
{
	return vb;
}

LPDIRECT3DINDEXBUFFER9 Model::GetIndexBuffer()
{
	return ib;
}

BoundingBox Model::GetBoundingBox()
{
	return bb;
}
