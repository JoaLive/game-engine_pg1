#include "stdafx.h"
#include "Material.h"
#include "Game.h"
#include <string>

void Material::SetTexture(LPDIRECT3DTEXTURE9 _tex)
{
	tex = new Texture(_tex);
	textureLoaded = true;
}

Texture * Material::GetTexture()
{
	return tex;
}

bool Material::IsTextureLoaded()
{
	return textureLoaded;
}

bool Material::IsEffectActivated()
{
	return effectActivated;
}

void Material::SetEffect(std::string effPath)
{

	D3DXCreateEffectFromFile(
		Game::GetInstance()->GetDevice(), L"efectoV2.fx", NULL, NULL,
		D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
		NULL, &effect, NULL);

	effectActivated = true;
}

LPD3DXEFFECT Material::GetEffect()
{
	return effect
;
}

void Material::SetAmbientColor(D3DXVECTOR4 col)
{
	ambientColor = col;
}

D3DXVECTOR4 Material::GetAmbientColor()
{
	return ambientColor;
}

Material::Material()
{
	effectActivated = false;
	ambientColor = { 0.1f, 0.1f, 0.1f, 0 };
}

Material::Material(LPDIRECT3DTEXTURE9 _tex)
{
	tex = new Texture(_tex);
	textureLoaded = true;
	effectActivated = false;

	ambientColor = { 0.1f, 0.1f, 0.1f, 0 };
}

Material::~Material()
{

	ambientColor = { 0.1f, 0.1f, 0.1f, 0 };
}


void Material::SetEffectsValues()
{
	if (intMap.size() > 0)
	{
		std::map<std::string, int>::iterator intIter;
		for (intIter = intMap.begin(); intIter != intMap.end(); ++intIter)
			effect->SetInt(intIter->first.data(), intIter->second);
	}

	if (floatMap.size() > 0)
	{
		std::map<std::string, float>::iterator floatIter;
		for (floatIter = floatMap.begin(); floatIter != floatMap.end(); ++floatIter)
			effect->SetFloat(floatIter->first.data(), floatIter->second);
	}

	if (vectorMap.size() > 0)
	{
		std::map<std::string, D3DXVECTOR4>::iterator vectorIter;
		for (vectorIter = vectorMap.begin(); vectorIter != vectorMap.end(); ++vectorIter)
			effect->SetVector(vectorIter->first.data(), &vectorIter->second);
	}

	if (matrixMap.size() > 0)
	{
		std::map<std::string, D3DXMATRIX>::iterator matrixIter;
		for (matrixIter = matrixMap.begin(); matrixIter != matrixMap.end(); ++matrixIter)
			effect->SetMatrix(matrixIter->first.data(), &matrixIter->second);
	}

}

void Material::SetIntToEffect(std::string name, int value)
{
	intMap.insert(std::pair<std::string, int>(name, value));
}

void Material::SetFloatToEffect(std::string name, float value)
{
	floatMap.insert(std::pair<std::string, float>(name, value));
}

void Material::SetVectorToEffect(std::string name, D3DXVECTOR4 value)
{
	vectorMap.insert(std::pair<std::string, D3DXVECTOR4>(name, value));
}

void Material::SetMatrixToEffect(std::string name, D3DXMATRIX value)
{
	matrixMap.insert(std::pair<std::string, D3DXMATRIX>(name, value));
}

