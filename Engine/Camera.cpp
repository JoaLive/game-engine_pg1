#include "stdafx.h"
#include "Camera.h"
#include "Plane.h"

void Camera::Update(LPDIRECT3DDEVICE9 dev)
{
	UpdateViewMat();
	dev->SetTransform(D3DTS_VIEW, &viewMat);
	dev->SetTransform(D3DTS_PROJECTION, &proyMat);

}

D3DXMATRIX Camera::GetViewMat()
{
	return viewMat;
}

D3DXMATRIX Camera::GetProyMat()
{
	return proyMat;
}

D3DXVECTOR4 Camera::GetPosition()
{
	return position;
}

void Camera::UpdateViewMat()
{
	viewMat = traslateMat * (rotXMat * rotYMat * rotZMat);
}

void Camera::SetMatrixToIden()
{
	D3DXMatrixIdentity(&traslateMat);
	D3DXMatrixIdentity(&rotXMat);
	D3DXMatrixIdentity(&rotYMat);
	D3DXMatrixIdentity(&rotZMat);
	D3DXMatrixIdentity(&viewMat);
	D3DXMatrixIdentity(&proyMat);
}

void Camera::RecalculatePlanes()
{
	D3DMATRIX mp = viewMat * proyMat;

	planes[0]->a = mp._14 - mp._11;
	planes[0]->b = mp._24 - mp._21;
	planes[0]->c = mp._34 - mp._31;
	planes[0]->d = mp._44 - mp._41;

	planes[1]->a = mp._14 + mp._11;
	planes[1]->b = mp._24 + mp._21;
	planes[1]->c = mp._34 + mp._31;
	planes[1]->d = mp._44 + mp._41;

	planes[2]->a = mp._12 + mp._14;
	planes[2]->b = mp._22 + mp._24;
	planes[2]->c = mp._32 + mp._34;
	planes[2]->d = mp._42 + mp._44;

	planes[3]->a = mp._14 - mp._12;
	planes[3]->b = mp._24 - mp._22;
	planes[3]->c = mp._34 - mp._32;
	planes[3]->d = mp._44 - mp._42;

	planes[4]->a = mp._13;
	planes[4]->b = mp._23;
	planes[4]->c = mp._33;
	planes[4]->d = mp._43;

	planes[5]->a = mp._14 - mp._13;
	planes[5]->b = mp._24 - mp._23;
	planes[5]->c = mp._34 - mp._33;
	planes[5]->d = mp._44 - mp._43;

	for (int i = 0; i < 6; i++)
		planes[i]->Normalize();

	dirtyFlag = false;

	/*
	rightPlane->a = mp._14 - mp._11;
	rightPlane->b = mp._24 - mp._21;
	rightPlane->c = mp._34 - mp._31;
	rightPlane->d = mp._44 - mp._41;

	leftPlane->a = mp._14 + mp._11;
	leftPlane->b = mp._24 + mp._21;
	leftPlane->c = mp._34 + mp._31;
	leftPlane->d = mp._44 + mp._41;

	bottomPlane->a = mp._12 + mp._14;
	bottomPlane->b = mp._22 + mp._24;
	bottomPlane->c = mp._32 + mp._34;
	bottomPlane->d = mp._42 + mp._44;

	topPlane->a = mp._14 - mp._12;
	topPlane->b = mp._24 - mp._22;
	topPlane->c = mp._34 - mp._32;
	topPlane->d = mp._44 - mp._42;

	nearPlane->a = mp._13;
	nearPlane->b = mp._23;
	nearPlane->c = mp._33;
	nearPlane->d = mp._43;

	farPlane->a = mp._14 - mp._13;
	farPlane->b = mp._24 - mp._23;
	farPlane->c = mp._34 - mp._33;
	farPlane->d = mp._44 - mp._43;
	*/
}

Camera::Camera() : dirtyFlag(false)
{
	SetMatrixToIden();
	position = { 0, 0, 0, 0 };

	D3DMATRIX mp = viewMat * proyMat;

	planes[0] = new Plane(mp._14 - mp._11, mp._24 - mp._21, mp._34 - mp._31, mp._44 - mp._41);
	planes[1] = new Plane(mp._14 + mp._11, mp._24 + mp._21, mp._34 + mp._31, mp._44 + mp._41);
	planes[2] = new Plane(mp._12 + mp._14, mp._22 + mp._24, mp._32 + mp._34, mp._42 + mp._44);
	planes[3] = new Plane(mp._14 - mp._12, mp._24 - mp._22, mp._34 - mp._32, mp._44 - mp._42);
	planes[4] = new Plane(mp._13, mp._23, mp._33, mp._43);
	planes[5] = new Plane(mp._14 - mp._13, mp._24 - mp._23, mp._34 - mp._33, mp._44 - mp._43);
	
	for (int i = 0; i < 6; i++)
		planes[i]->Normalize();

	/*
	rightPlane = new Plane(mp._14 - mp._11, mp._24 - mp._21, mp._34 - mp._31, mp._44 - mp._41);
	leftPlane = new Plane(mp._14 + mp._11, mp._24 + mp._21, mp._34 + mp._31, mp._44 + mp._41);
	bottomPlane = new Plane(mp._12 + mp._14, mp._22 + mp._24, mp._32 + mp._34, mp._42 + mp._44);
	topPlane = new Plane(mp._14 - mp._12, mp._24 - mp._22, mp._34 - mp._32, mp._44 - mp._42);
	nearPlane = new Plane(mp._13, mp._23, mp._33, mp._43);
	farPlane = new Plane(mp._14 - mp._13, mp._24 - mp._23, mp._34 - mp._33, mp._44 - mp._43);
	*/
}


Camera::~Camera()
{
	//delete[] planes;
}

void Camera::Traslate(D3DXVECTOR4 pos)
{
	position = pos;
	traslateMat._41 = -pos.x;
	traslateMat._42 = -pos.y;
	traslateMat._43 = -pos.z;
	dirtyFlag = true;
}

void Camera::Rotate(D3DXVECTOR3 rot)
{
	rotXMat._22 = cos(-rot.x);
	rotXMat._32 = -sin(-rot.x);
	rotXMat._23 = sin(-rot.x);
	rotXMat._33 = cos(-rot.x);

	rotYMat._11 = cos(-rot.y);
	rotYMat._31 = -sin(-rot.y);
	rotYMat._13 = sin(-rot.y);
	rotYMat._33 = cos(-rot.y);

	rotZMat._11 = cos(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._22 = cos(-rot.z);
	dirtyFlag = true;
}

void Camera::SetPerspective(FLOAT FOV, FLOAT minDist, FLOAT maxDist)
{
	D3DXMatrixPerspectiveFovLH(&proyMat, D3DXToRadian(FOV), (float)640 / 480, minDist, maxDist);
	dirtyFlag = true;
}
