#ifndef INPUT__H
#define INPUT__H

#include <dinput.h>
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
#include <map>
#include "stdafx.h"

class Input
{
private:
	static Input* instance;
	LPDIRECTINPUTDEVICE8 keyDev;
	byte currentKeys[256];
	byte prevKeys[256];
	std::map<std::string, std::vector<int>*> keyMap;
public:
	Input();
	~Input();
	static Input* GetInstance();
	void Initialize(HINSTANCE hInstance, HWND hWnd);
	void ActualizeKeys();
	void Release();
	bool KeyDown(int key);
	bool KeyDown(std::vector<int>* keys);
	bool KeyUp(int key);
	bool KeyUp(std::vector<int>* keys);
	bool KeyPressed(int key);
	bool KeyPressed(std::vector<int>* keys);
	void AddKeyMap(std::string code, int key);
	std::vector<int>* GetKeyMap(std::string code);
};





#endif // !INPUT__H
