#include "stdafx.h"
#include "Composite.h"

Composite::Composite()
{
}

Composite::~Composite()
{
}

void Composite::AddComponent(Component * component)
{
	components.push_back(component);
	component->SetParent(this);

	UpdateParentBBox();
}

void Composite::Remove(Component * component)
{
	for (std::vector<Component*>::iterator iter = components.begin(); iter != components.end(); ++iter)
	{
		if (*iter == component)
		{
			components.erase(iter);
			UpdateParentBBox();
			break;
		}
	}
}

void Composite::Update()
{
	UpdateComposite();

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Update();
}

void Composite::Render()
{
	D3DXMATRIX* model = UpdateMatrix();
	RenderComposite(model);

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Render(model);
}

void Composite::Render(D3DXMATRIX* parent)
{
	parentMat = &(*UpdateMatrix() /** *parent*/ );

	RenderComposite(parentMat);

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Render(parentMat);
}

D3DXMATRIX Composite::GetParentModelMat()
{
	D3DXMATRIX aux;
	if (GetParent())
		aux = GetParent()->GetParentModelMat() * *UpdateMatrix();
	else
		aux = *UpdateMatrix();

	return aux;
}

D3DXVECTOR3 Composite::GetWorldPosition()
{

	D3DXMATRIX transMat = GetParent() ? GetParentWorldPosition() : *UpdateMatrix();
	D3DXVECTOR3 wPos = { transMat._41, transMat._42, transMat._43 };

	return wPos;
}

D3DXVECTOR3 Composite::GetWorldRotation()
{
	UpdateMatrix();

	D3DXMATRIX rotX = GetParentWorldRotationX();
	D3DXMATRIX rotY = GetParentWorldRotationY();
	D3DXMATRIX rotZ = GetParentWorldRotationZ();

	float angX;
	float angY;
	float angZ;

	if (parentMat->_11 == 1.0f)
	{
		angX = atan2f(parentMat->_13, parentMat->_34);
		angY = 0;
		angZ = 0;

	}
	else if (parentMat->_11 == -1.0f)
	{
		angX = atan2f(parentMat->_13, parentMat->_34);
		angY = 0;
		angZ = 0;
	}
	else
	{

		angX = atan2(-parentMat->_31, parentMat->_11);
		angY = asin(parentMat->_21);
		angZ = atan2(-parentMat->_23, parentMat->_22);
	}

	D3DXVECTOR3 wRot = { angX, angY, angZ };
	

	return wRot;
}
BoundingBox Composite::GetCombinedBBox(D3DXMATRIX parentModelMat)
{
	if (components.size() == 0)
	{
		BoundingBox aux;
		aux = aux.Transform(parentModelMat * *UpdateMatrix());
		bb = aux;
		return bb;
	}

	BoundingBox aux;

	D3DXMATRIX UpdatedparentModelMat = parentModelMat * *UpdateMatrix();
	aux = aux.Transform(UpdatedparentModelMat);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(UpdatedparentModelMat));

	bb = aux;
	return bb;
}

void Composite::UpdateParentBBox()
{
	BoundingBox aux;

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetBoundingBox());
	
	bb = aux;

	if (GetParent() != NULL)
		GetParent()->UpdateParentBBox();
}


void Composite::Move(D3DXVECTOR3 dir)
{
	pos += dir;

	for (int i = 0; i < components.size(); i++)
		components[i]->Move(dir);

	//bb = bb.Transform(*UpdateMatrix());
	if (GetParent())
		GetParent()->UpdateParentBBox();
}

void Composite::Rotate(D3DXVECTOR3 angles)
{
	rot += angles;

	for (int i = 0; i < components.size(); i++)
		components[i]->Rotate(angles);

	BoundingBox aux;
	D3DXMATRIX parentModelMat = GetParentModelMat();
	aux = aux.Transform(parentModelMat);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(parentModelMat));

	bb = aux;
	if (GetParent())
		GetParent()->UpdateParentBBox();
}

void Composite::Scale(D3DXVECTOR3 mag)
{
	scale += mag;

	for (int i = 0; i < components.size(); i++)
		components[i]->Scale(mag);

	BoundingBox aux;
	D3DXMATRIX parentModelMat = GetParentModelMat();
	aux = aux.Transform(parentModelMat);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(parentModelMat));

	bb = aux;
	if (GetParent())
		GetParent()->UpdateParentBBox();
}

void Composite::SetPosition(D3DXVECTOR3 _pos)
{
	pos = _pos;

	for (int i = 0; i < components.size(); i++)
		components[i]->SetPosition(_pos);

	//bb = bb.Transform(*UpdateMatrix());
	if (GetParent())
		GetParent()->UpdateParentBBox();
}

void Composite::SetRotation(D3DXVECTOR3 angles)
{
	rot = angles;

	for (int i = 0; i < components.size(); i++)
		components[i]->SetRotation(angles);

	BoundingBox aux;
	D3DXMATRIX parentModelMat = GetParentModelMat();
	aux = aux.Transform(parentModelMat);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(parentModelMat));

	bb = aux;
	if (GetParent())
		GetParent()->UpdateParentBBox();
}

void Composite::SetScale(D3DXVECTOR3 mag)
{
	scale = mag;

	for (int i = 0; i < components.size(); i++)
		components[i]->SetScale(mag);

	BoundingBox aux;
	D3DXMATRIX parentModelMat = GetParentModelMat();
	aux = aux.Transform(parentModelMat);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(parentModelMat));

	bb = aux;
	if (GetParent())
		GetParent()->UpdateParentBBox();
}

void Composite::SeekImportantBB()
{
	if (components.size() == 0)
		UpdateParentBBox();
	else
	{
		for (int i = 0; i < components.size(); i++)
		{
			components[i]->SeekImportantBB();
		}
	}
}

D3DXMATRIX Composite::GetParentWorldPosition()
{
	D3DXMATRIX transMat = translateMat;

	if (GetParent() != NULL)
		transMat = *GetParent()->GetParentWorldPosition() * *UpdateMatrix();

	return transMat;
}

D3DXMATRIX Composite::GetParentWorldRotationX()
{
	UpdateMatrix();
	D3DXMATRIX rotX = rotXMat;

	if (GetParent() != NULL)
		rotX = GetParent()->GetParentWorldRotationX() * rotX;

	return rotX;
}

D3DXMATRIX Composite::GetParentWorldRotationY()
{
	UpdateMatrix();
	D3DXMATRIX rotY = rotYMat;

	if (GetParent() != NULL)
		rotY = GetParent()->GetParentWorldRotationY() * rotY;

	return rotY;
}

D3DXMATRIX Composite::GetParentWorldRotationZ()
{
	UpdateMatrix();
	D3DXMATRIX rotZ = rotZMat;

	if (GetParent() != NULL)
		rotZ = GetParent()->GetParentWorldRotationZ() * rotZ;

	return rotZ;
}

void Composite::UpdateComposite()
{
}

void Composite::RenderComposite(D3DXMATRIX* model)
{
}
