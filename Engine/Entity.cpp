#include "stdafx.h"
#include "Entity.h"



void Entity::UpdateModelMat()
{
	ModelMat = (scaleMat * (rotXMat * rotYMat * rotZMat) * traslateMat) * parentMat;
}

Entity::Entity()
{
	D3DXMatrixIdentity(&ModelMat);
	D3DXMatrixIdentity(&scaleMat);
	D3DXMatrixIdentity(&rotXMat);
	D3DXMatrixIdentity(&rotYMat);
	D3DXMatrixIdentity(&rotZMat);
	D3DXMatrixIdentity(&traslateMat);
	D3DXMatrixIdentity(&parentMat);



	position = { 0,0,0 };

	forward = { 0,0,1 };
	right = { 1,0,0 };
	up = { 0,1,0 };


}

Entity::Entity(D3DXVECTOR3 pos)
{
	position = pos;
}


Entity::~Entity()
{
	position = { 0, 0, 0 };
}

void Entity::Draw(LPDIRECT3DDEVICE9 dev)
{
	
	material->GetTexture()->SetTextureSettings(dev);

	UpdateModelMat();

	dev->SetTransform(D3DTS_WORLD, &ModelMat);

	dev->SetFVF(CUSTOMFVF);

	dev->SetStreamSource(0, material->GetModel()->GetVertexBuffer(), 0, sizeof(Vertex));

	dev->SetIndices(material->GetModel()->GetIndexBuffer());

	dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 6, 0, 2);
}

D3DXVECTOR3 Entity::GetPosition()
{
	return position;
}

D3DXVECTOR3 Entity::GetForward()
{
	return forward;
}

D3DXVECTOR3 Entity::GetRight()
{
	return right;
}

D3DXVECTOR3 Entity::GetUp()
{
	return up;
}

Material * Entity::GetMaterial()
{
	return material;
}

void Entity::SetPosition(D3DXVECTOR3 pos)
{
	traslateMat._41 = pos.x;
	traslateMat._42 = pos.y;
	traslateMat._43 = pos.z;
}

void Entity::Rotate(D3DXVECTOR3 rot)
{
	rotXMat._22 = cos(rot.x);
	rotXMat._23 = sin(rot.x);
	rotXMat._32 = -sin(rot.x);
	rotXMat._33 = cos(rot.x);

	rotYMat._11 = cos(rot.y);
	rotYMat._31 = sin(rot.y);
	rotYMat._13 = -sin(rot.y);
	rotYMat._33 = cos(rot.y);

	rotZMat._11 = cos(rot.z);
	rotZMat._21 = sin(rot.z);
	rotZMat._12 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);

	D3DXVECTOR3 wForward(0, 0, 1);
	D3DXVECTOR3 wRight(1, 0, 0);
	D3DXVECTOR3 wUp(0, 1, 0);

	D3DXVECTOR4 objForward;
	D3DXVECTOR4 objRight;
	D3DXVECTOR4 objUp;

	D3DXVec3Transform(&objForward, &wForward, &(rotXMat * rotYMat * rotXMat));
	forward = {objForward.x, objForward.y, objForward.z };

	D3DXVec3Transform(&objRight, &wRight, &(rotXMat * rotYMat * rotXMat));
	right = { objRight.x, objRight.y, objRight.z };

	D3DXVec3Transform(&objUp, &wUp, &(rotXMat * rotYMat * rotXMat));
	up = { objUp.x, objUp.y, objUp.z };
}

void Entity::Scale(D3DXVECTOR3 sca)
{
	scaleMat._11 = sca.x;
	scaleMat._22 = sca.y;
	scaleMat._33 = sca.z;
}

void Entity::SetMaterial(Material* _mat)
{
	material = _mat;
}

D3DXMATRIX Entity::GetmodelMat()
{
	return ModelMat;
}

void Entity::MakeChildOf(Entity parent)
{
	parentMat = parent.GetmodelMat();
}
