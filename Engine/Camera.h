#ifndef CAMERA__H
#define CAMERA__H

#include "EngineApi.h"
#include "Game.h"
#include "stdafx.h"

class Plane;

class ENGINE_API Camera
{
private:
	D3DXVECTOR4 position;
	D3DXMATRIX traslateMat;
	D3DXMATRIX rotXMat;
	D3DXMATRIX rotYMat;
	D3DXMATRIX rotZMat;
	D3DXMATRIX viewMat;
	D3DXMATRIX proyMat;
	void UpdateViewMat();
	void SetMatrixToIden();
	void RecalculatePlanes();
	/*
	Plane* rightPlane;
	Plane* leftPlane;
	Plane* topPlane;
	Plane* bottomPlane;
	Plane* nearPlane;
	Plane* farPlane;
	*/
	Plane* planes[6];
	bool dirtyFlag;
public:
	Camera();
	~Camera();
	void Traslate(D3DXVECTOR4 pos);
	void Rotate(D3DXVECTOR3 rot);
	void SetPerspective(FLOAT FOV, FLOAT minDist, FLOAT maxDist);
	void Update(LPDIRECT3DDEVICE9 dev);
	D3DXMATRIX GetViewMat();
	D3DXMATRIX GetProyMat();
	D3DXVECTOR4 GetPosition();
};

#endif // !CAMERA__H



