#ifndef MESHRENDERER__H
#define MESHRENDERER__H

#include "Composite.h"

class Material;
class Model;

class MeshRenderer : public Composite
{
private:
	Material* material;
	Model* model;

public:
	MeshRenderer(Material* _material);
	~MeshRenderer();
	void UpdateComposite() override;
	void RenderComposite(D3DXMATRIX* model) override;
	void SetMaterial(Material* _mat);
	void SetModel(Model* _mod);
	BoundingBox GetCombinedBBox(D3DXMATRIX parentModelMat); 
	void UpdateParentBBox();

	void Move(D3DXVECTOR3 dir);
	void Rotate(D3DXVECTOR3 angles);
	void Scale(D3DXVECTOR3 mag);
	
	void SetRotation(D3DXVECTOR3 angles);
	void SetScale(D3DXVECTOR3 mag);
};

#endif // !MESHRENDERER__H



