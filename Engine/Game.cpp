#include "stdafx.h"
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include <chrono>
#include "Composite.h"
#include "MeshRenderer.h"
#include "TilemapRender.h"
#include "AnimationComponent.h"
#include "Animation.h"
#include "Frame.h"
#include "Material.h"
#include "LightManager.h"
#include "DirectionalLight.h"
#include "SpotLight.h"
#include "PointLight.h"
#include "Texture.h"

using namespace std;
using namespace std::chrono;

Game* Game::instance = NULL;

Game::Game(){
	windowHeight = 480;
	windowWidth = 640;
	instance = this;
}

Game::~Game(){}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


D3DXMATRIX GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = pos.x;
	transMat._42 = pos.y;
	transMat._43 = pos.z;

	D3DXMATRIX scaMat;
	D3DXMatrixIdentity(&scaMat);
	scaMat._11 = sca.x;
	scaMat._22 = sca.y;
	scaMat._33 = sca.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(rot.z);
	rotZMat._12 = sin(rot.z);
	rotZMat._21 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);

	return scaMat * rotZMat * transMat;
};

D3DXMATRIX GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = -pos.x;
	transMat._42 = -pos.y;
	transMat._43 = -pos.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._22 = cos(-rot.z);

	return transMat * rotZMat;
};

int Game::GetMS()
{
	time_point<system_clock> actualTime = system_clock::now();
	system_clock::duration duration = actualTime.time_since_epoch();
	milliseconds ms = duration_cast<milliseconds>(duration);
	return ms.count();
}

float Game::GetDeltaTime()
{
	return deltaTime;
}

Camera * Game::GetMainCam()
{
	return mainCam;
}

void Game::Run(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow)
{
	//Creamos la clase de la ventana
	WNDCLASSEX wcex;

	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));


	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

											 //Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	HWND hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"Title", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		windowWidth, //Ancho
		windowHeight, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

	//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	//Creo la interfaz con la placa de video

	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de vido
		D3DDEVTYPE_HAL, //Soft o hard
		hWnd, //Ventana
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&dev); //El device que se crea


	//dev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	dev->SetRenderState(D3DRS_LIGHTING, false);
	dev->SetRenderState(D3DRS_ZENABLE, true);
	dev->SetRenderState(D3DRS_ZWRITEENABLE, true);
	dev->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);


	Camera cam;
	cam.SetPerspective(60, 0.3f, 500);
	cam.Traslate({ 0, 0, -1, 0 });

	mainCam = &cam;

	Input::GetInstance()->Initialize(hInstance, hWnd);

	//CARGA DE MODELOS
	Model modelCube("cube.obj", "bus");
	//
	LPDIRECT3DTEXTURE9 tex1 = CreateTexture(L"water.jpg");
	LPDIRECT3DTEXTURE9 tex2 = CreateTexture(L"metal.jpg");

	Material mat1(tex1);
	Material mat2(tex2);

	//SHADERS Y LUCES
	mat1.SetEffect("shaderTexTinte.fx");
	mat1.SetAmbientColor({ 0.1f, 0.3f, 0.1f,0 });

	mat2.SetEffect("shaderTexTinte.fx");
	mat2.SetAmbientColor({ 0.1f, 0.3f, 0.1f,0 });

	DirectionalLight directLight;
	directLight.SetColor({ 1, 1, 1, 0 });
	directLight.SetDirecction({ 0, 0, 1, 0 });
	//LightManager::GetInstance()->AddLight(&directLight);

	SpotLight spotLight;
	spotLight.SetPosition({ 0.5f, 0, 1 });
	spotLight.SetColor({ 0, 0, 1, 0 });
	spotLight.SetDirecction({ 0, 0, 1, 0 });
	spotLight.SetRange(5);
	LightManager::GetInstance()->AddLight(&spotLight);

	SpotLight spotLight2;
	spotLight2.SetPosition({ -0.5f, 0, 1 });
	spotLight2.SetColor({ 0, 1, 0, 0 });
	spotLight2.SetDirecction({ 0, 0, 1, 0 });
	spotLight2.SetRange(5);
	LightManager::GetInstance()->AddLight(&spotLight2);
	
	PointLight pointLight;
	pointLight.SetPosition({ 0, 0, 1 });
	pointLight.SetColor({ 1, 1, 1, 0 });
	pointLight.SetDirecction({ 0, 0, 1, 0 });
	pointLight.SetRange(5);
	//LightManager::GetInstance()->AddLight(&pointLight);
	
	//ANIMACIONES
	/*
	Animation anim1("walk", &matAnim, 320, 320);
	anim1.AddFrame(new Frame(64, 64, 0, 0));
	anim1.AddFrame(new Frame(64, 64, 64, 0));
	anim1.AddFrame(new Frame(64, 64, 128, 0));
	anim1.AddFrame(new Frame(64, 64, 192, 0));
	anim1.AddFrame(new Frame(64, 64, 256, 0));
	anim1.AddFrame(new Frame(64, 64, 0, 64));

	Animation anim2("wave", &matAnim, 320, 320);
	anim2.AddFrame(new Frame(64, 64, 64, 64));
	anim2.AddFrame(new Frame(64, 64, 128, 64));
	anim2.AddFrame(new Frame(64, 64, 192, 64));
	anim2.AddFrame(new Frame(64, 64, 256, 64));


	Animation anim3("attack", &matAnim, 320, 320);
	anim3.AddFrame(new Frame(64, 64, 0, 128));
	anim3.AddFrame(new Frame(64, 64, 64, 128));
	anim3.AddFrame(new Frame(64, 64, 128, 128));
	anim3.AddFrame(new Frame(64, 64, 192, 128));
	anim3.AddFrame(new Frame(64, 64, 256, 128));
	anim3.AddFrame(new Frame(64, 64, 0, 192));
	anim3.AddFrame(new Frame(64, 64, 64, 192));
	anim3.AddFrame(new Frame(64, 64, 128, 192));
	anim3.AddFrame(new Frame(64, 64, 192, 192));
	anim3.AddFrame(new Frame(64, 64, 256, 192));
	anim3.AddFrame(new Frame(64, 64, 0, 256));
	anim3.AddFrame(new Frame(64, 64, 64, 256));
	anim3.AddFrame(new Frame(64, 64, 128, 256));

	AnimationComponent animComp;
	anim1.SetSpeed(10);
	anim2.SetSpeed(10);
	anim3.SetSpeed(10);
	animComp.AddAnimation(&anim1);
	animComp.AddAnimation(&anim2);
	animComp.AddAnimation(&anim3);
	animComp.Play("walk");
	//
	*/

	MeshRenderer mesh1(&mat1);
	mesh1.SetModel(&modelCube);

	MeshRenderer mesh2(&mat2);
	mesh2.SetModel(&modelCube);
	
	//TILEMAP
	vector<vector<vector<int>>> tileMap =
	{
		{
			{ 1,0,1,0,1 },
			{ 0,1,0,1,0 },
			{ 1,0,1,0,1 },
		},
		{
			{-1,-1,-1,-1,-1},
			{2,-1,2,-1,2},
			{-1,2,-1,2,-1 },
		}
	};
	TilemapRender tileMesh(tileMap, 0.5f, 0.5f);
	Texture texA(tex1);
	tileMesh.AddTexture(&texA);

	Composite entidad1;
	Composite entidad2;

	entidad1.AddComponent(&mesh1);
	entidad1.Move({ 0.5f,0.5f,1 });

	entidad2.AddComponent(&mesh2);
	entidad2.Move({ -0.5f,-0.5f,1 });

	entidad2.AddComponent(&entidad1);

	//entidad2.Rotate({ 0, 3.14f, 0 });

	int lastFrameMs = GetMS();
	float minFrameTime = 0.0167f;

	float num = 0;

	while (true)
	{
		actualMs = GetMS();
		deltaTime = (actualMs - lastFrameMs) / 1000.0f;

		lastFrameMs = actualMs;

		MSG msg;

	

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
		{
			break;
		}

		cam.Update(dev);
		Input::GetInstance()->ActualizeKeys();

		//Actualizar
		dev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 100, 0, 100), 1.0f, 0);
		dev->BeginScene();
		//Update();


		num += 0.05f;

		spotLight.Move({ 0, 0, cos(num) * 0.02f });
		spotLight2.Move({ 0, 0, cos(num + 180) * 0.02f });
		
		//entidad1.Move({ -0.01f, 0, 0 });

		entidad1.Update();
		entidad1.Render();

		entidad2.Update();
		entidad2.Render();


		entidad1.GetBoundingBox();
		entidad2.GetBoundingBox();

		/*
		if (Input::GetInstance()->KeyDown(DIK_1))
		{
			animComp.Stop();
			animComp.Play("walk");
		}
		else if (Input::GetInstance()->KeyDown(DIK_2))
		{
			animComp.Stop();
			animComp.Play("wave");
		}
		else if (Input::GetInstance()->KeyDown(DIK_3))
		{
			animComp.Stop();
			animComp.Play("attack");
		}
		else if (Input::GetInstance()->KeyPressed(DIK_D))
		{
			entidad3.Move({ 0.01f, 0, 0 });
		}
		else if (Input::GetInstance()->KeyPressed(DIK_A))
		{
			entidad3.Move({-0.01f, 0, 0 });
		}
		else if (Input::GetInstance()->KeyPressed(DIK_W))
		{
			entidad3.Move({ 0, 0.01f, 0 });
		}
		else if (Input::GetInstance()->KeyPressed(DIK_S))
		{
			entidad3.Move({ 0, -0.01f, 0 });
		}
		*/
		dev->EndScene();
		dev->Present(NULL, NULL, NULL, NULL);
	}
	dev->Release();
	d3d->Release();
}


void Game::CreateBuffers(LPDIRECT3DVERTEXBUFFER9 * vb, int vertexCount, LPDIRECT3DINDEXBUFFER9 * ib, int indexCount)
{
	dev->CreateVertexBuffer(
		vertexCount * sizeof(Vertex),
		D3DUSAGE_WRITEONLY, 
		CUSTOMFVF,				
		D3DPOOL_MANAGED,	
		vb,				
		NULL);				

	dev->CreateIndexBuffer(
		indexCount * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		ib,
		NULL);


}

Game * Game::GetInstance()
{
	if (instance == NULL)
		instance = new Game;
	return instance;
}

LPDIRECT3DDEVICE9 Game::GetDevice()
{
	return dev;
}

LPDIRECT3DTEXTURE9 Game::CreateTexture(wchar_t*  direction)
{
	LPDIRECT3DTEXTURE9 tex;
	D3DXCreateTextureFromFile(dev, direction, &tex);
	return tex;
}

//Manejo de mensajes por ventana
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hWnd, message, wParam, lParam);
}