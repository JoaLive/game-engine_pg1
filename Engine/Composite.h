#ifndef COMPOSITE__H
#define COMPOSITE__H

#include "stdafx.h"
#include "Component.h"

using namespace std;

class Composite : public Component
{
private:
	template<class T> void AddComponentToVectorInParent(vector<T*>* vec);
	D3DXMATRIX GetParentWorldPosition();
	D3DXMATRIX GetParentWorldRotationX();
	D3DXMATRIX GetParentWorldRotationY();
	D3DXMATRIX GetParentWorldRotationZ();
protected:
	D3DXMATRIX* parentMat;
	vector<Component*> components;
	virtual void UpdateComposite();
	virtual void RenderComposite(D3DXMATRIX* model);

public:
	Composite();
	virtual ~Composite();

	void AddComponent(Component* component);
	void Remove(Component* component);
	void Update() override final;
	void Render() override final;
	void Render(D3DXMATRIX* parent) override final;
	D3DXMATRIX GetParentModelMat();
	D3DXVECTOR3 GetWorldPosition();
	D3DXVECTOR3 GetWorldRotation();
	template<class T> T* GetComponent();
	template<class T> T* GetComponentInParent();
	template<class T> vector<T*> GetComponentsInParent();
	template<class T> T* GetComponentInChildren();
	virtual BoundingBox GetCombinedBBox(D3DXMATRIX parentModelMat);
	void UpdateParentBBox();

	virtual void Move(D3DXVECTOR3 dir);
	virtual void Rotate(D3DXVECTOR3 angles);
	virtual void Scale(D3DXVECTOR3 mag);

	virtual void SetPosition(D3DXVECTOR3 _pos);
	virtual void SetRotation(D3DXVECTOR3 angles);
	virtual void SetScale(D3DXVECTOR3 mag);

	virtual void SeekImportantBB();
};

template<class T>
inline T * Composite::GetComponent()
{
	for (size_t i = 0; i < components.size(); i++)
	{
		T* comp = dynamic_cast<T*>(components[i]);
		if (comp != NULL)
			return comp;
	}
	return NULL;
}

template<class T>
inline T * Composite::GetComponentInParent()
{
	T* comp = dynamic_cast<T*>(this);

	if (comp != NULL)
		return comp;

	else if (GetParent() != NULL)
		return GetParent()->GetComponentInParent<T>();

	return NULL;
}

template<class T>
inline vector<T*> Composite::GetComponentsInParent()
{
	vector<T*> vec; 

	AddComponentToVectorInParent(&vec);

	return vec;
}

template<class T>
inline T * Composite::GetComponentInChildren()
{
	T* comp = dynamic_cast<T*>(this);
	if (comp != NULL)
		return comp;
	
	for (size_t i = 0; i < components.size(); i++)
	{
		Composite* compositeChild = dynamic_cast<Composite*>(components[i]);

		if (compositeChild != NULL)
		{	
			T* childComp = compositeChild->GetComponentInChildren<T>();
			if (childComp != NULL)
				return childComp;
		}
		else
		{
			T* childComp = dynamic_cast<T*>(child);
			if (childComp != NULL)
				return childComp;
		}
	}
	return NULL;
}

template<class T>
inline void Composite::AddComponentToVectorInParent(vector<T*>* vec)
{
	T* comp = dynamic_cast<T*>(this);

	if (comp != NULL)
		vec->push_back(this);

	if (GetParent() != NULL)
		GetParent()->AddComponentToVectorInParent(vec);
}

#endif // !COMPOSITE__H 