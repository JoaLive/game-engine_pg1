#ifndef ANIMATIONCOMPONENT__H
#define ANIMATIONCOMPONENT__H

#include "stdafx.h"
#include "Composite.h"

using namespace std;

class Animation;

class AnimationComponent : public Composite
{
private:
	vector<Animation*> animations;
	Animation* actAnimation;

public:
	AnimationComponent();
	~AnimationComponent();
	void UpdateComposite() override;
	void AddAnimation(Animation* anim);
	void Play(string tag);
	void Pause();
	void Stop();
};

#endif // !ANIMATIONCOMPONENT__H


