#ifndef GAME__H
#define GAME__H

#include "EngineApi.h"
#include "stdafx.h"

class Camera;

class ENGINE_API Game
{
protected:
	int windowHeight;
	int windowWidth;
private:
	float actualMs;
	float deltaTime;
	static Game* instance;
	LPDIRECT3DDEVICE9 dev;
	Camera* mainCam;
public:
	Game();
	~Game();
	void Run(_In_     HINSTANCE hInstance,
		_In_opt_ HINSTANCE hPrevInstance,
		_In_     LPTSTR    lpCmdLine,
		_In_     int       nCmdShow);
	void CreateBuffers(LPDIRECT3DVERTEXBUFFER9* vb, int vertexCount, LPDIRECT3DINDEXBUFFER9* ib, int indexCount);
	static Game* GetInstance();
	LPDIRECT3DDEVICE9 GetDevice();
	LPDIRECT3DTEXTURE9 CreateTexture(wchar_t*  direction);
	int GetMS();
	float GetDeltaTime();
	Camera* GetMainCam();
};

#endif // !GAME__H