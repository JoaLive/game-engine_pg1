#ifndef COMPONENT__H
#define COMPONENT__H

#include "stdafx.h"
#include "BoundingBox.h"

class Composite;

class Component
{
private:
	Composite* parent;

protected:
	D3DXMATRIX translateMat;
	D3DXMATRIX rotXMat;
	D3DXMATRIX rotYMat;
	D3DXMATRIX rotZMat;
	D3DXMATRIX scaleMat;
	D3DXMATRIX ModelMat;

	D3DXVECTOR3 forward;
	D3DXVECTOR3 right;
	D3DXVECTOR3 up;

	D3DXVECTOR3 pos;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 rot;

	BoundingBox bb;

public:
	Component();
	~Component();

	virtual void Update();
	virtual void Render();
	virtual void Render(D3DXMATRIX* parent);

	virtual void Move(D3DXVECTOR3 dir);
	virtual void Rotate(D3DXVECTOR3 angles);
	virtual void Scale(D3DXVECTOR3 mag);

	virtual void SetPosition(D3DXVECTOR3 _pos);
	virtual void SetRotation(D3DXVECTOR3 angles);
	virtual void SetScale(D3DXVECTOR3 mag);

	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();
	D3DXVECTOR3 GetScale();

	void SetParent(Composite* parent);
	Composite* GetParent();

	D3DXMATRIX* UpdateMatrix();

	D3DXVECTOR3 GetForward();
	D3DXVECTOR3 GetRight();
	D3DXVECTOR3 GetUp();

	virtual BoundingBox GetBoundingBox();
	virtual BoundingBox GetCombinedBBox(D3DXMATRIX parentModelMat);
	//virtual void UpdateParentBBox();
	virtual void SeekImportantBB();
};


#endif // !COMPONENT__H