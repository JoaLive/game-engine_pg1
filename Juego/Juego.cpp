
#include "stdafx.h"
#include "Juego.h"
#include "..\Engine\Game.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	Game juego;
	juego.Run(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}
