struct VS_INPUT
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	float3 normal : TEXCOORD2;
	float4 worldPos : TEXCOORD3;
};

const int DIRECTIONAL_LIGHT = 0;
const int POINT_LIGHT = 1;
const int SPOT_LIGHT = 2;

float4x4 modelMat;
float4x4 mvpMat;
float4x4 rotMat;
float3 camPos;
int lightType;
float lightRange;
float3 lightDir;
float3 lightPos;
float4 lightCol;
float4 ambientCol;
sampler2D tex : register(s0);

float4 getDiffuseLightDir(float3 normal, float4 worldPos)
{
	float3 lDir = lightDir;

	float diffuse = max(0, dot(lDir, normal));
	
	float3 eyeDir = normalize(camPos - worldPos);
	float3 specularDir = reflect(lDir, normal);
	float specularFactor = max(0, dot(eyeDir, specularDir));
	specularFactor = pow(specularFactor, 3);
	float4 specularColor = specularFactor * float4(1, 1, 1, 1);
	
	return (lightCol * diffuse);
}

float4 getDiffuseLightSpot(float3 normal, float4 worldPos)
{
	float3 lDir = lightDir;
	float dist = 0;

	float3 diff = lightPos - worldPos;
	lDir = normalize(diff);
	dist = length(diff);
	

	float diffuse = max(0, dot(lDir, normal));
	float falloff = 1 - dist / lightRange;

	float spot = dot(lDir, lightDir);
	if (spot < 0.707) 
		falloff = 0;
	
	//Todo esto calcula los reflejos specular
	float3 eyeDir = normalize(camPos - worldPos);
	float3 specularDir = reflect(lDir, normal);
	float specularFactor = max(0, dot(eyeDir, specularDir));
	specularFactor = pow(specularFactor, 3);
	float4 specularColor = specularFactor * float4(1, 1, 1, 1);

	return (lightCol * diffuse * falloff);
}

float4 getDiffuseLightPoint(float3 normal, float4 worldPos)
{
	float3 lDir = lightDir;
	float dist = 0;

	float3 diff = lightPos - worldPos;
	lDir = normalize(diff);
	dist = length(diff);

	float diffuse = max(0, dot(lDir, normal));
	float falloff = 1 - dist / lightRange;

	float3 eyeDir = normalize(camPos - worldPos);
	float3 specularDir = reflect(lDir, normal);
	float specularFactor = max(0, dot(eyeDir, specularDir));
	specularFactor = pow(specularFactor, 3);
	float4 specularColor = specularFactor * float4(1, 1, 1, 1);

	return (lightCol * diffuse * falloff);
}


VS_OUTPUT VS(VS_INPUT vertex)
{
	VS_OUTPUT fragment;
	fragment.position = mul(vertex.position, mvpMat);
	fragment.normal = mul(vertex.normal, rotMat);
	fragment.uv = vertex.uv;
	fragment.worldPos = mul(vertex.position, modelMat);
	return fragment;
}


float4 PSDirBase(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = ambientCol + getDiffuseLightDir(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;

	//return float4(fragment.normal, 1);
}

float4 PSSpotBase(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = ambientCol + getDiffuseLightSpot(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;

	//return float4(fragment.normal, 1);
}

float4 PSPointBase(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = ambientCol + getDiffuseLightPoint(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;

	//return float4(fragment.normal, 1);
}

float4 PSDirAdd(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = getDiffuseLightDir(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;

	//return float4(fragment.normal, 1);
}

float4 PSSpotAdd(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = getDiffuseLightSpot(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;
	
	//return float4(fragment.normal, 1);
}

float4 PSPointAdd(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = getDiffuseLightPoint(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;

	//return float4(fragment.normal, 1);
}

technique Efecto
{
	pass pDirBase
	{
		AlphaBlendEnable = false;
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSDirBase();
	}

	pass pSpotBase
	{
		AlphaBlendEnable = false;
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSSpotBase();
	}

	pass pPointBase
	{
		AlphaBlendEnable = false;
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSPointBase();
	}	
	
	
	pass pDirAdd
	{
		AlphaBlendEnable = true;
		DestBlend = One;
		SrcBlend = One;
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSDirAdd();
	}

	pass pSpotAdd
	{
		AlphaBlendEnable = true;
		DestBlend = One;
		SrcBlend = One;
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSSpotAdd();
	}

	pass pPointAdd
	{
		AlphaBlendEnable = true;
		DestBlend = One;
		SrcBlend = One;
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSPointAdd();
	}
}